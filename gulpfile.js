'use strict';

const { series, parallel, src, dest, watch } = require('gulp');
const clean = require('gulp-clean');
const sass = require('gulp-sass');
sass.compiler = require('node-sass');

function cleanDist() {
  return src('dist', {
    read: false,
    allowEmpty: true
  })
    .pipe(clean());
}

function compileScss() {
  return src('src/styles/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(dest('dist/styles'));
}

function copyToDist() {
  return src([
    'src/images/**/*',
    'src/**/*.html'
  ], { base: "./src" })
    .pipe(dest('dist/'));
}

function buildWatch() {
  build();
  watch('src/styles/**/*.scss', compileScss);
  watch([
    'src/images/**/*',
    'src/**/*.html'
  ], copyToDist);
}

const build = series(cleanDist, parallel(compileScss, copyToDist));

exports.build = build;
exports.buildWatch = buildWatch;
exports.default = build;
